package org.jeecg.config.tenant;

import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;

/**
 * @Author jianghx
 * @Date 2022/3/3 15:15
 * @Version 1.0
 **/
@Component
public class MyPathMatchingFilterChainResolver extends PathMatchingFilterChainResolver {


    public String  getMyPathWithinApplication(ServletRequest request){
        return super.getPathWithinApplication(request);
    }

    public boolean MyPathMatches2(String pattern, String path) {
        return super.pathMatches(pattern,path);
    }
}

