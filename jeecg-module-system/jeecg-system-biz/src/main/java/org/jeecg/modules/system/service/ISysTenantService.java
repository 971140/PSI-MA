package org.jeecg.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.SysTenant;

import java.util.Collection;
import java.util.List;

public interface ISysTenantService extends IService<SysTenant> {
    /**
     * 保存
     * @param sysTenant
     */
    void saveSysTenant(SysTenant sysTenant);
    /**
     * 查询有效的租户
     *
     * @param idList
     * @return
     */
    List<SysTenant> queryEffectiveTenant(Collection<Integer> idList);
}
